from django.db import models
from django.utils.translation import gettext_lazy as _
import uuid



class PrintingOrderStates(models.TextChoices):
    INIT = "INIT", _("INIT")
    TO_DOWNLOAD = "TO_DOWNLOAD", _("TO DOWNLOAD")
    READY = "READY", _("READY")
    PENDING = "PENDING",  _("PENDING")
    FINISHED = "FINISHED", _("FINISHED")
    PAUSED = "PAUSED", _("PAUSED")
    FAILED = "FAILED", _("FAILED")


class PrintingOrderCutOptions(models.TextChoices):
    NONE = "NONE", _("WITHOUT CUT")
    BEFORE = "BEFORE", _("BEFORE")
    AFTER = "AFTER", _("AFTER")
    BEFORE_EACH = "BEFORE_EACH", _("BEFORE EACH")
    AFTER_EACH = "AFTER_EACH", _("AFTER EACH")


class PrintingOrder(models.Model):
    uuid = models.UUIDField(verbose_name="UUID", default=uuid.uuid4, unique=True, editable=False)
    file = models.FileField(verbose_name=_("File"))
    file_url = models.URLField(null=True, blank=True, verbose_name=_("File URL"))
    state = models.CharField(
        verbose_name=_("State"),
        max_length=16,
        choices=PrintingOrderStates.choices,
        default=PrintingOrderStates.INIT
    )
    n_to_print = models.IntegerField(verbose_name=_("Number to print"), default=1)
    n_printed = models.IntegerField(verbose_name=_("Number printed"), default=0)
    cut = models.CharField(verbose_name=_("Cut mode"), max_length=32, choices=PrintingOrderCutOptions.choices,
                           default=PrintingOrderCutOptions.NONE)
    cut_feed = models.IntegerField(verbose_name=_("Cut feed"), default=6)
    external_server = models.ForeignKey('puller.ExternalServer', verbose_name=_("External server"), null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Printing order")
        verbose_name_plural = _("Printing orders")
        constraints = [
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_state_valid",
                check=models.Q(state__in=PrintingOrderStates.values),
            )
        ]

    def __repr__(self):
        return str(self.uuid)

    def __str__(self):
        return self.__repr__()

    def print(self, printer: 'src.printer.Printer', max_batch=None):
        i = 0
        if self.n_printed == 0 and self.cut == PrintingOrderCutOptions.BEFORE:
            printer.cut(feed=self.cut_feed>0, feed_lines=self.cut_feed)
        while self.n_to_print > self.n_printed:
            if self.cut == PrintingOrderCutOptions.BEFORE_EACH:
                printer.cut(feed=self.cut_feed>0, feed_lines=self.cut_feed)
            printer.image(self.file.path)
            if self.cut == PrintingOrderCutOptions.AFTER_EACH:
                printer.cut(feed=self.cut_feed>0, feed_lines=self.cut_feed)
            i += 1
            self.n_printed += 1
            if max_batch and i > max_batch:
                break
        if self.n_printed == self.n_to_print and self.cut == PrintingOrderCutOptions.AFTER:
            printer.cut(feed=self.cut_feed>0, feed_lines=self.cut_feed)
