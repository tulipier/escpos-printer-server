from django.contrib import admin
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _

from django_admin_multi_select_filter.filters import MultiSelectFieldListFilter


from .models import PrintingOrder


@admin.register(PrintingOrder)
class PrintingOrderAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'state', 'file_tag', 'cut', 'n_to_print', 'external_server')
    list_filter = [
        ('state', MultiSelectFieldListFilter),
        'external_server'
    ]

    @admin.display(
        description=_("File"),
    )
    def file_tag(self, obj):
        if obj.file:
            return format_html('<img src="{0}" style="max-height:80px; max-width: 80px;" />'.format(obj.file.url))

