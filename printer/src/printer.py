import escpos.printer
import escpos.image
from escpos.constants import GS, PAPER_FULL_CUT, PAPER_PART_CUT
import six


class Printer(escpos.printer.Escpos):
    def image(self, img_source, fragment_height=4, impl="graphics", resize=True, *args, **kwargs):
        if resize:
            img = escpos.image.EscposImage(img_source)
            if img.width > self.img_max_width:
                print("Resizing image ...")
                ratio = img.width/self.img_max_width
                img_source = img._im.resize((self.img_max_width, int(img.height/ratio)))
        super().image(img_source, fragment_height=fragment_height, impl=impl)

    def cut(self, mode='FULL', feed=True, feed_lines=6):
        """ Cut paper.

        Without any arguments the paper will be cut completely. With 'mode=PART' a partial cut will
        be attempted. Note however, that not all models can do a partial cut. See the documentation of
        your printer for details.

        :param mode: set to 'PART' for a partial cut. default: 'FULL'
        :param feed: print and feed before cutting. default: true
        :raises ValueError: if mode not in ('FULL', 'PART')
        """

        if not feed:
            self._raw(GS + b'V' + six.int2byte(66) + b'\x00')
            return

        self.print_and_feed(feed_lines)

        mode = mode.upper()
        if mode not in ('FULL', 'PART'):
            raise ValueError("Mode must be one of ('FULL', 'PART')")

        if mode == "PART":
            if self.profile.supports('paperPartCut'):
                self._raw(PAPER_PART_CUT)
            elif self.profile.supports('paperFullCut'):
                self._raw(PAPER_FULL_CUT)
        elif mode == "FULL":
            if self.profile.supports('paperFullCut'):
                self._raw(PAPER_FULL_CUT)
            elif self.profile.supports('paperPartCut'):
                self._raw(PAPER_PART_CUT)


class UsbPrinter(escpos.printer.Usb, Printer):
    def __init__(self, *args, img_max_width=None, **kwargs):
        escpos.printer.Usb.__init__(self, *args, **kwargs)
        self.img_max_width = img_max_width if img_max_width is not None else \
        self.profile.profile_data['media']['width']['pixels']


class UsbEPSON_TM_T88V(UsbPrinter):
    def __init__(self, *args, **kwargs):
        super().__init__(0x04b8, 0x0202, profile="TM-T88V", *args, **kwargs)


PRINTER_DICT = {
    "TM-T88V": {
        "USB": {
            "class": UsbEPSON_TM_T88V
        }
    }
}


def get_printer(model: str, link="USB", *args, **kwargs) -> Printer:
    """
    Return a printer base on model and link mode (default to USB)
    :param model:
    :param link:
    :return:
    """
    return PRINTER_DICT[model][link]['class'](*args, **kwargs)


def get_printer_from_config(config):
    if config['printer__model'] != '':
        return get_printer(model=config['printer__model'], link=config['printer__link'])
    else:
        pass