class SyncPrinter:
    _printer = None

    @classmethod
    def get_printer(cls, fnct, *args, **kwargs):
        if cls._printer is None:
            try:
                cls._printer = fnct(*args, **kwargs)
                cls._printer.hw('INIT')
            except Exception as e:
                print(f"{e}")
                cls._printer = None
        else:
            try:
                cls._printer.is_online()
            except Exception as e:
                print(f"{e}")
                cls._printer = None
        return cls._printer