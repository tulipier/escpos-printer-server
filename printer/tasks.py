from celery import shared_task
from django.core.cache import cache

from .src.printer import get_printer_from_config
from .src.sync_printer import SyncPrinter
from dynamic_preferences.registries import global_preferences_registry

from .models import PrintingOrder, PrintingOrderStates


@shared_task(queue='printer')
def printer_state():
    context = dict()
    config = global_preferences_registry.manager()
    printer = SyncPrinter.get_printer(get_printer_from_config, config)
    context['printer_model'] = config['printer__model']
    context['printer_link'] = config['printer__link']
    context['printer_connected'] = printer.is_online() if printer is not None else False
    context['printer_paper'] = printer.paper_status() if printer is not None else None
    cache.set('printer_state', context, timeout=30)
    return context


@shared_task(queue='printer')
def printer_print():
    config = global_preferences_registry.manager()
    printer = SyncPrinter.get_printer(get_printer_from_config, config)
    while True:
        order = PrintingOrder.objects.filter(state=PrintingOrderStates.READY)[:1]
        if len(order) == 0:
            break
        order = order[0]
        order.state = PrintingOrderStates.PENDING
        order.save(force_update=True, update_fields=['state', ])
        try:
            order.print(printer)
        except Exception as e:
            order.state = PrintingOrderStates.FAILED
            order.save(force_update=True, update_fields=['state', 'n_printed'])
        else:
            if order.n_printed >= order.n_to_print:
                order.state = PrintingOrderStates.FINISHED
                order.save(force_update=True, update_fields=['state', 'n_printed'])

