from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import ChoicePreference, StringPreference
from dynamic_preferences.preferences import Section

from django.db import models
from django.utils.translation import gettext_lazy as _

from .src.printer import PRINTER_DICT

PrinterModels = models.TextChoices('PrinterModels',
                                   {
    model: model for model in PRINTER_DICT.keys()
                                   })
PrinterLinks = models.TextChoices('PrinterLinks',
                                   'USB')

printer = Section('printer', verbose_name=_("Printer"))

@global_preferences_registry.register
class PrinterModel(ChoicePreference):
    section = printer
    verbose_name = _("Printer model")
    name = 'model'
    default = PrinterModels.choices[0][0]
    choices = PrinterModels.choices + [
        ('', _('-- other --'))
    ]


@global_preferences_registry.register
class PrinterLink(ChoicePreference):
    section = printer
    verbose_name = _("Printer link")
    name = 'link'
    default = PrinterLinks.choices[0][0]
    choices = PrinterLinks.choices


@global_preferences_registry.register
class PrinterUSBVendor(StringPreference):
    section = printer
    verbose_name = _("Printer USB idVendor")
    name = 'usb_id_vendor'
    default = ''


@global_preferences_registry.register
class PrinterUSBProduct(StringPreference):
    section = printer
    verbose_name = _("Printer USB idProduct")
    name = 'usb_id_product'
    default = ''
