from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class PrinterConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "printer"
    verbose_name = _("Printer")
