#!/usr/bin/bash

LOG=${1:-"INFO"}
BIND=${2:-"127.0.0.1:9000"}

echo "Log Level : ${LOG} — Bind to : ${BIND}"

echo "Start main celery worker"
celery -A escpos_printer_server worker -l ${LOG} --scheduler django_celery_beat.schedulers:DatabaseScheduler -B &
PID_CELERY_MAIN=$!

echo "Start printer celery worker"
celery -A escpos_printer_server worker -l ${LOG} --concurrency=1 -Q printer &
PID_CELERY_PRINTER=$!

echo "Start webserver"
python manage.py runserver ${BIND}

echo "Stopping main celery worker"
kill ${PID_CELERY_MAIN}

echo "Stopping printer celery worker"
kill ${PID_CELERY_PRINTER}
