��          �      ,      �     �  	   �     �     �  	   �  	   �     �     �     �     �                =     I  	   W     a  A  |     �  	   �  %   �     �     �     
               '     /     M     k     �     �     �  +   �                                      	                      
                  Breadcrumbs Connected ESCPOS Printer Dashboard Home Last pull Link mode Low Missing Model Number of failed print order Number of finished print order Number of waiting print order Paper level Printer state Printings Pull from external servers Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Chemin Connecté Imprimante ESCPOS — Tableau de bord Accueil Dernier pull Mode de liaison Bas Manquant Modèle Nombre d'impression échouée Nombre d'impression terminée Nombre d'impression en attente Niveau du papier État de l'imprimante Impressions Récupération depuis les serveurs externes 