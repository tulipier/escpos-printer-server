from django.apps import AppConfig
import django.contrib.admin.apps


class AdminConfig(django.contrib.admin.apps.AdminConfig):
    default_site = 'dashboard.admin.AdminSite'


class DashboardConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "dashboard"
