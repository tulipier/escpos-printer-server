from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .views import IndexView

class AdminSite(admin.AdminSite):
    site_title = _("ESCPOS Printer")
    site_header = _("ESCPOS Printer")
    index_title = _("ESCPOS Printer Dashboard")
    index_template = IndexView.template_name
    def index(self, request, extra_context=None):
        app_list = self.get_app_list(request)

        context = {
            **self.each_context(request),
            "title": self.index_title,
            "subtitle": None,
            "app_list": app_list,
            **(extra_context or {}),
        }

        request.current_app = self.name
        return IndexView(request=request, extra_context=context).dispatch(request)

    def get_app_list(self, request, app_label=None):
        """
        Reorder apps in admin
        """
        all_list_default = super().get_app_list(request)
        all_list_dict = {app["app_label"]: app for app in all_list_default}
        force_order = ['printer', 'puller', 'dynamic_preferences']
        order = force_order + [app_label for app_label in all_list_dict.keys() if app_label not in force_order]
        all_list_sorted = [all_list_dict[app_label] for app_label in order if app_label in all_list_dict]
        return all_list_sorted
