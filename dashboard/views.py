from django.views.generic import TemplateView
from django.core.cache import cache

import printer.models

class IndexView(TemplateView):
    template_name = "dashboard/index.html"


class PrinterState(TemplateView):
    template_name = "dashboard/htmx/printer_state.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'printer_state' in cache:
            context.update(cache.get('printer_state'))
        return context


class PullerState(TemplateView):
    template_name = "dashboard/htmx/puller_state.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'last_external_server' in cache:
            context['last_external_server'] = cache.get('last_external_server')
        else:
            context['last_external_server'] = "xx"
        return context


class PrinterPrintState(TemplateView):
    template_name = "dashboard/htmx/printing_state.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['printing_ready'] = printer.models.PrintingOrder.objects.filter(
            state=printer.models.PrintingOrderStates.READY).count()
        context['printing_finished'] = printer.models.PrintingOrder.objects.filter(
            state=printer.models.PrintingOrderStates.FINISHED).count()
        context['printing_failed'] = printer.models.PrintingOrder.objects.filter(
            state=printer.models.PrintingOrderStates.FAILED).count()
        return context