from django.urls import path, include

from .views import PrinterState, PrinterPrintState, PullerState

app_name = 'dashboard'
urlpatterns = [
    path("htmx/printer/state", PrinterState.as_view(), name='printer_state'),
    path("htmx/printer/printing/state", PrinterPrintState.as_view(), name='printer_print_state'),
    path("htmx/puller/state", PullerState.as_view(), name='puller_state'),
]