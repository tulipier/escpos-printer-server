#!/bin/bash

echo "Creating virtual environment"
python -m venv venv
echo "Entering virtual environment"
source ./venv/bin/activate

echo "Installing dependencies"
./venv/bin/pip install -r requirements.txt

echo "Django : makemigrations"
./venv/bin/python manage.py makemigrations
echo "Django : migrate"
./venv/bin/python manage.py migrate
echo "Django : createcachetable"
./venv/bin/python manage.py createcachetable
echo "Django : createsuperuser"
./venv/bin/python manage.py createsuperuser

echo ""
echo "Installation complete"
echo "You now just need to run the server : python manage.py runserver"
