import os
import sys
import pathlib

from celery import Celery

# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'escpos_printer_server.settings')

basedir = pathlib.Path(os.path.dirname(os.path.realpath(sys.argv[0]))).parent

app = Celery('escpos_printer_server',
             # broker='sqla+sqlite:///' + os.path.join(basedir, 'celery.db'),
             # backend='db+sqlite:///' + os.path.join(basedir, 'celery_results.db')
)

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django apps.
app.autodiscover_tasks()


@app.task(bind=True, ignore_result=True)
def debug_task(self):
    print(f'Request: {self.request!r}')



app.conf.beat_schedule = {
    'external-server-pull': {
        'task': 'puller.tasks.pull_external_server',
        'schedule': 5.0,
        'args': (),
        'options': {
            'expires': 9.0
        }
    },
    'printer-state': {
        'task': 'printer.tasks.printer_state',
        'schedule': 5.0,
        'args': (),
        'options': {
            'expires': 9.0
        }
    },
    'printer-print': {
        'task': 'printer.tasks.printer_print',
        'schedule': 2.0,
        'args': (),
        'options': {
            'expires': 3.5
        }
    },
}