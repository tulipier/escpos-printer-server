from django.shortcuts import render
from django.views import View
from django.http import HttpResponse
from django.utils.translation import gettext_lazy as _

import datetime

from .models import ExternalServer, ExternalServerStates

# Create your views here.

# TODO : delete because no more needed ?
class PullerView(View):
    def get(self, request, *args, **kwargs):
        for server in ExternalServer.objects.exclude(state=ExternalServerStates.PAUSED):
            server.pull()
        return HttpResponse(f"{_('Last pull')} : {datetime.datetime.now().strftime('%Y-%M-%d %H:%m:%S')}")
