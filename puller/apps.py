from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class PullerConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "puller"
    verbose_name = _("Puller")
