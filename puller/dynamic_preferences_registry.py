from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import IntegerPreference
from dynamic_preferences.preferences import Section

from django.utils.translation import gettext_lazy as _

puller = Section('puller', verbose_name=_("Puller"))


@global_preferences_registry.register
class PullerPreDownload(IntegerPreference):
    section = puller
    verbose_name = _("Number of files to pre-download")
    name = 'n_pre_download'
    default = 5
