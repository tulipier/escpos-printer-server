import uuid as uuid
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import now

import requests
from dynamic_preferences.registries import global_preferences_registry

from printer.models import PrintingOrder, PrintingOrderStates, PrintingOrderCutOptions


class ExternalServerStates(models.TextChoices):
    INIT = "INI", _("INIT")
    READY = "RDY", _("READY")
    PAUSED = "PSD", _("PAUSED")
    FAILED = "FLD", _("FAILED")


class ExternalServer(models.Model):
    uuid = models.UUIDField(verbose_name="UUID", default=uuid.uuid4, unique=True, editable=False)
    name = models.CharField(verbose_name=_("Server name"), unique=True, max_length=128)
    api_url = models.URLField(verbose_name=_("API URL"))
    api_token = models.CharField(verbose_name=_("API Token"), max_length=512)
    last_seen = models.DateTimeField(verbose_name=_("Last seen"), blank=True, null=True)
    last_recv = models.DateTimeField(verbose_name=_("Last received"), blank=True, null=True)
    state = models.CharField(
        verbose_name=_("State"),
        max_length=3,
        choices=ExternalServerStates.choices,
        default=ExternalServerStates.INIT
    )
    state_details = models.CharField(verbose_name=_("State"), max_length=256, default="", null=True, blank=True)

    class Meta:
        verbose_name = _("External server")
        verbose_name_plural = _("External servers")
        constraints = [
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_state_valid",
                check=models.Q(state__in=ExternalServerStates.values),
            )
        ]

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.__repr__()

    def pull(self):
        try:
            response = requests.get(f"{self.api_url}/pull?from={self.uuid}", headers={'Authorization': f'Token {self.api_token}'})
            if response.status_code != 200:
                print(f"Error in pull [{response.status_code}] : {response.reason}")
                self.state = ExternalServerStates.FAILED
                self.state_details = f"[{response.status_code}] {response.reason}"
                self.save()
                return False
            else:
                self.last_seen = now()
            data = response.json()
            printing_orders_to_create = list()
            server_uuids = PrintingOrder.objects.filter(external_server=self).values_list('uuid', flat=True)
            for server_order in data['new_orders']:
                if server_order['uuid'] not in server_uuids:
                    order = PrintingOrder(
                        uuid=server_order['uuid'],
                        file_url=f"{self.api_url}/download/{server_order['uuid']}",
                        state=PrintingOrderStates.TO_DOWNLOAD,
                        external_server=self,
                        n_to_print=server_order.get('n_to_print', 1),
                        cut=server_order.get('cut', PrintingOrderCutOptions.NONE),
                        cut_feed=server_order.get('cut_feed', 6),
                    )
                    printing_orders_to_create.append(order)
            if len(printing_orders_to_create) > 0:
                self.last_recv = now()
                PrintingOrder.objects.bulk_create(printing_orders_to_create)
            if len(data['wait_updates']) > 0:
                orders_dict = {order.uuid: order.state for order in
                               PrintingOrder.objects.filter(uuid__in=data['wait_updates'])}
                requests.post(f"{self.api_url}/update?from={self.uuid}", data=orders_dict,
                              headers={'Authorization': f'Token {self.api_token}'})
            print(data)
            self.state = ExternalServerStates.READY
            self.state_details = f"[{response.status_code}] {response.reason}"
            self.save()
        except requests.exceptions.ConnectionError as e:
            self.state = ExternalServerStates.FAILED
            self.state_details = _("Unable to connect")
            self.save()
            return e

    def pre_donwload(self):
        config = global_preferences_registry.manager()
        for order in PrintingOrder.objects.filter(
                state=PrintingOrderStates.TO_DOWNLOAD,
                external_server=self
        )[:config['puller__n_pre_download']]:
            print(f"Try to download : {order}")
            try:
                order.file.save(str(order.uuid),
                                requests.get(order.file_url, headers={'Authorization': f'Token {self.api_token}'}, stream=True).raw)
            except Exception as e:
                raise e
            else:
                order.state=PrintingOrderStates.READY
                order.save()

