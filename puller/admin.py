from django.contrib import admin
from django_admin_multi_select_filter.filters import MultiSelectFieldListFilter

from .models import ExternalServer


@admin.register(ExternalServer)
class ExternalServerAdmin(admin.ModelAdmin):
    list_display = ('name', 'state', 'api_url')
    list_filter = [
        ('state', MultiSelectFieldListFilter),
    ]
    readonly_fields = ['state', 'state_details', 'last_seen', 'last_recv']

    class Media:
        js = [
            'js/htmx.min.js',
        ]

