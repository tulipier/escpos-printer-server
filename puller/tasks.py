from celery import shared_task
from django.core.cache import cache
import datetime

from .models import ExternalServer, ExternalServerStates


@shared_task()
def pull_external_server():
    print("Pull external server")
    for server in ExternalServer.objects.exclude(state=ExternalServerStates.PAUSED):
        server.pull()
        server.pre_donwload()
    cache.set('last_external_server', datetime.datetime.now(), timeout=None)