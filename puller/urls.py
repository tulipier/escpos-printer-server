from django.urls import path, include

from .views import PullerView

app_name = 'puller'
urlpatterns = [
    path("refresh", PullerView.as_view(), name='refresh')
]